#include "WaterArea.h"
#include <iostream>

using namespace std;

WaterArea::WaterArea() 
{
	name = "";
	area = 0;
	lat = 0;
	lon = 0;
	deep = 0;
}
WaterArea::WaterArea(double area) : WaterArea()
{
	this->area = area;
}
WaterArea::WaterArea(double lat, double lon) : WaterArea()
{
	setGeo(lat, lon);
}
WaterArea::WaterArea(string name, double area, double lat, double lon, double deep) : WaterArea()
{
	setName(name);
	setArea(area);
	setGeo(lat, lon);
	setDeep(deep);
}

void WaterArea::setName(string name) 
{
	this->name = name;
}
void WaterArea::setArea(double area) 
{
	this->area = area;
}

double WaterArea::getArea() {
	return this->area;
}
void WaterArea::setGeo(double lat, double lon) 
{
	this->lat = lat;
	this->lon = lon;
}
void WaterArea::setDeep(double deep)
{
	this->deep = deep;
}

std::ostream& operator<< (std::ostream& out, WaterArea& w) {
	out << w.print();

	return out;
}
std::istream& operator>> (std::istream& in, WaterArea& w) {
	string name;
	double area;
	double lat, lon;
	double deep;

	in >> name;
	in >> area;
	in >> lat;
	in >> lon;
	in >> deep;
	w.setName(name);
	w.setArea(area);
	w.setGeo(lat, lon);
	w.setDeep(deep);

	return in;
}
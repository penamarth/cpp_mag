#pragma once
#include "Cone.h"
#include <iostream>

class ConeFrustum : Cone
{
	double R;
	double H;

public:
	ConeFrustum();
	ConeFrustum(double R, double r, double H, double h);
	ConeFrustum(double X, double Y, double R, double r, double H, double h);

	void setCone(double X, double Y, double R, double r, double H, double h);

	double surfArea();
	double Volume();

	friend std::ostream& operator<< (std::ostream& out, ConeFrustum& c);
	friend std::istream& operator>> (std::istream& in, ConeFrustum& c);
};

std::ostream& operator<< (std::ostream& out, ConeFrustum& c);
std::istream& operator>> (std::istream& in, ConeFrustum& c);

bool operator> (ConeFrustum& c1, ConeFrustum& c2);

bool operator<  (ConeFrustum& c1, ConeFrustum& c2);
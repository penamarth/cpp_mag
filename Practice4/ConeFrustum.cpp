#include "ConeFrustum.h"
#include <cmath>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <iostream>
using namespace std;

ConeFrustum::ConeFrustum() : Cone()
{
	R = 0;
	H = 0;
}
ConeFrustum::ConeFrustum(double R, double r, double H, double h) : Cone(r,h)
{
	this->R = R;
	this->H = H;
}
ConeFrustum::ConeFrustum(double X, double Y, double R, double r, double H, double h) : Cone(X,Y,r,H)
{
	this->R = R;
	this->H = H;
}

void ConeFrustum::setCone(double X, double Y, double R, double r, double H, double h)
{
	Cone::setCone(X,Y,r,h);
	this->R = R;
	this->H = H;
}

double ConeFrustum::surfArea() 
{
	double L = sqrt(pow(R, 2) + pow(H + Cone::H, 2));

	return M_PI * R * (R + L) - this->Cone::surfArea() +  2 * M_PI * pow(R, 2);
}
double ConeFrustum::Volume() 
{
	return 1.0 / 3 * M_PI * (H + Cone::H) * pow(R, 2) - Cone::Volume();
}

std::ostream& operator<< (std::ostream& out, ConeFrustum& c) {
	out << "�����: " << c.Cone::centerX << " " << c.Cone::centerY << endl;
	out << "������ �����: " << c.Cone::r << endl;
	out << "������ �������: " << c.R << endl;
	out << "������: " << c.H << endl;
	out << "������� �����������: " << c.surfArea() << endl;
	out << "�����: " << c.Volume() << endl;

	return out;
}
std::istream& operator>> (std::istream& in, ConeFrustum& c) {
	double X, Y, R, r, H, h;

	cin >> X >> Y >> R >> r >> H >> h;
	c.setCone(X, Y, R, r, H, h);

	return in;

}

bool operator> (ConeFrustum& c1, ConeFrustum& c2) {
	return c1.Volume() > c2.Volume();
}

bool operator<  (ConeFrustum& c1, ConeFrustum& c2) {
	return c1.Volume() < c2.Volume();
}
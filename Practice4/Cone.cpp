#include "Cone.h"
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
using namespace std;

Cone::Cone() {
	centerX = 0;
	centerY = 0;
	r = 0;
	H = 0;
}
Cone::Cone(double R, double H) {
	Cone::centerX = 0;
	Cone::centerY = 0;
	Cone::r = R;
	Cone::H = H;
}
Cone::Cone(double X, double Y, double R, double H) {
	Cone::centerX = X;
	Cone::centerY = Y;
	Cone::r = R;
	Cone::H = H;
}
void Cone::setCone(double X, double Y, double R, double H) {
	Cone::centerX = X;
	Cone::centerY = Y;
	Cone::r = R;
	Cone::H = H;
}
void Cone::print() {
	cout << "�����: " << centerX << " " << centerY << endl;
	cout << "������: " << r << endl;
	cout << "������: " << H << endl;
	cout << "������� �����������: " << surfArea() << endl;
	cout << "�����: " << Volume() << endl;
}
double Cone::surfArea() {

	double l = sqrt(pow(r, 2) + pow(H, 2));

	return M_PI * r * (r + l);
}
double Cone::Volume() {
	return 1.0 / 3 * M_PI * H * pow(r, 2);
}
#include "Cone.h"
#include "ConeFrustum.h"
#include "Ocean.h"
#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");

	/*������� 1*/
	
	Cone a(1, 1), b(1, 1, 1, 1);

	a.print();

	b.print();

	Cone* dyn = new Cone();

	dyn->setCone(2,2,2,2);
	dyn->print();

	delete dyn;

	Cone* arr[3];

	for (int i = 0; i < 3; i++)
	{
		arr[i] = new Cone(1 + i, 1 + i, 1 + i, 1 + i);
	}

	for (int i = 0; i < 3; i++)
	{
		arr[i]->print();
	}

	for (int i = 0; i < 3; i++)
	{
		delete arr[i];
	}
	
	/*������� 2*/
	
	ConeFrustum *cf1 = new ConeFrustum();

	cin >> *cf1;

	cout << *cf1;

	ConeFrustum* cf2 = new ConeFrustum();

	cin >> *cf2;

	cout << (*cf1 < *cf2 ? "������ ������" : "������ ������") << endl;

	cout << (*cf1 > *cf2 ? "������ ������" : "������ ������") << endl;

	
	/*������� 3*/

	WaterArea *bay1 = (WaterArea *) new Bay();

	cin >> *bay1;

	cout << *bay1 << endl;


	vector<WaterArea*> bays;

	bays.push_back((WaterArea*)new Bay());
	bays.push_back((WaterArea*)new Bay());
	bays.push_back((WaterArea*)new Bay());

	bays[0]->setName("������������� �����");
	bays[0]->setArea(700);
	bays[0]->setGeo(44,50);
	bays[0]->setDeep(10);

	bays[1]->setName("���������� �����");
	bays[1]->setArea(800);
	bays[1]->setGeo(44, 46);
	bays[1]->setDeep(1.5);

	bays[2]->setName("���������� �����");
	bays[2]->setArea(693);
	bays[2]->setGeo(36, 53);
	bays[2]->setDeep(4);


	WaterArea* sea = (WaterArea*) new Sea("���������� ����", bays, 390000.,42.,51.,1025.);

	cout << *sea << endl;

	vector<WaterArea*> bays2;

	bays2.push_back((WaterArea*)new Bay("��������� �����"));
	bays2[0]->setArea(35000);
	bays2[0]->setGeo(8,-79);
	bays2[0]->setDeep(100);
	bays2.push_back((WaterArea*)new Bay("���������� �����"));
	bays2[1]->setArea(11000);
	bays2[1]->setGeo(55,162);
	bays2[1]->setDeep(2000);
	vector<WaterArea*> seas;

	seas.push_back((WaterArea*)new Sea("�������� ����", 1603000, 54, 148, 3916));


	Bay *bay = new Bay("����������� �����");
	bay->setArea(12000);
	bay->setGeo(53, 141);
	bay->setDeep(40);

	((Sea *)seas[0])->AddBay(*bay);
	seas.push_back((WaterArea*)new Sea("��������� ����", 2260000, 60, 178, 4151));

	bay = new Bay("����� ��������");
	bay->setArea(10000);
	bay->setGeo(60, 157);
	bay->setDeep(350);

	((Sea*)seas[0])->AddBay(*bay);

	seas.push_back((WaterArea*)new Sea("�������� ����", 1062000, 39, 134, 3742));

	bay = new Bay("����� ����� ��������");
	bay->setArea(9000);
	bay->setGeo(42, 131);
	bay->setDeep(200);

	((Sea*)seas[1])->AddBay(*bay);

	WaterArea* ocean = (WaterArea*) new Ocean("����� �����", bays2, seas, 178684000., -18., -152., 10994);

	cout << *ocean << endl;

	return 0;
}

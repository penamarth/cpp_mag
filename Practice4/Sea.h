#pragma once
#include "WaterArea.h"
#include "Bay.h"
#include <vector>

using namespace std;

class Sea : public WaterArea
{
	vector<WaterArea *> internalBays;

public:
	string print();
	void AddBay(Bay &b);

	double getArea();

	Sea();
	Sea(vector<Bay *> bays);
	Sea(string name, double area = 0.0, double lat = 0.0, double lon = 0.0, double deep = 0.0);
	Sea(string name, vector<WaterArea*> bays, double area = 0.0, double lat = 0.0, double lon = 0.0, double deep = 0.0);

};
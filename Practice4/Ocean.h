#pragma once
#include "WaterArea.h"
#include "Sea.h"
#include "Bay.h"
#include <vector>
#include <string>

using namespace std;

class Ocean : public WaterArea
{
	vector<WaterArea *> internalSeas;
	vector<WaterArea *> internalBays;

public:
	string print();

	void AddBay(Bay &b);
	void AddSea(Sea &s);

	double getArea();

	Ocean();
	Ocean(string name, double area = 0.0, double lat = 0.0, double lon = 0.0, double deep = 0.0);
	Ocean(string name, vector<WaterArea*> bays, vector<WaterArea*> seas, double area = 0.0, double lat = 0.0, double lon = 0.0, double deep = 0.0);

};
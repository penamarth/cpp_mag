#pragma once
#include <string>
#include <iostream>

using namespace std;

class WaterArea
{
protected:
	string name;
	double area;
	double lat, lon;
	double deep;

public:
	WaterArea();
	WaterArea(double area);
	WaterArea(double lat, double lon);
	WaterArea(string name, double area=0, double lat=0.0, double lon=0.0, double deep=0.0);

	void setName(string name);
	void setArea(double area);
	void setGeo(double lat, double lon);
	void setDeep(double deep);
	virtual string print() = 0;
	virtual double getArea();
};

std::ostream& operator<< (std::ostream& out, WaterArea& w);
std::istream& operator>> (std::istream& in, WaterArea& w);
#pragma once

class Cone
{
protected:
	double centerX, centerY;
	double r;
	double H;

public:
	Cone();
	Cone(double R, double H);
	Cone(double X, double Y, double R, double H);
	void setCone(double X, double Y, double R, double H);
	void print();
	double surfArea();
	double Volume();
};
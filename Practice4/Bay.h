#pragma once
#include "WaterArea.h"

#include <string>
using namespace std;

class Bay : public WaterArea
{
public:
	string print();

	Bay();
	Bay(string name, double area = 0.0, double lat = 0.0, double lon = 0.0, double deep = 0.0);
};
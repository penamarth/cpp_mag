#include "Ocean.h"

string Ocean::print()
{
	string res = "�����: " + name + "\n" +
		"�������: " + std::to_string(getArea()) + "\n" +
		"������: " + std::to_string(lat) + " " + "�������: " + std::to_string(lon) + "\n" +
		"�������: " + std::to_string(deep) + "\n\n" +
		"���������� ����: \n";

	for (int i = 0; i < internalSeas.size(); i++)
	{
		res += internalSeas[i]->print();
	}
	for (int i = 0; i < internalBays.size(); i++)
	{
		res +=  internalBays[i]->print();
	}
	res += "\n";
	return res;
}

void Ocean::AddBay(Bay &b)
{
	internalBays.push_back((WaterArea*)&b);
}
void Ocean::AddSea(Sea &s)
{
	internalSeas.push_back((WaterArea*)&s);
}

double Ocean::getArea()
{
	double result = this->area;

	for (int i = 0; i < internalBays.size(); i++)
	{
		result += internalBays[i]->getArea();
	}

	for (int i = 0; i < internalSeas.size(); i++)
	{
		result += internalSeas[i]->getArea();
	}
	return result;
}


Ocean::Ocean() : WaterArea()
{

}

Ocean::Ocean(string name, double area, double lat, double lon, double deep) : WaterArea(name, area, lat, lon, deep)
{

}
Ocean::Ocean(string name, vector<WaterArea*> bays, vector<WaterArea*> seas, double area, double lat, double lon, double deep) : WaterArea(name, area, lat, lon, deep)
{
	this->internalBays = bays;
	this->internalSeas = seas;
}

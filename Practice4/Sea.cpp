#include "WaterArea.h"
#include "Sea.h"

string Sea::print() 
{
	string res = "����: " + name + "\n" +
		"�������: " + std::to_string(getArea()) + "\n" +
		"������: " + std::to_string(lat) + " " + "�������: " + std::to_string(lon) + "\n" +
		"�������: " + std::to_string(deep) + "\n\n" +
		"���������� �����: \n\n";

	for (int i = 0; i < internalBays.size(); i++)
	{
		res += internalBays[i]->print();
	}
	res += "\n";
	return res;
}

double Sea::getArea()
{
	double result = this->area;

	for (int i = 0; i < internalBays.size(); i++)
	{
		result += internalBays[i]->getArea();
	}
	return result;
}

void Sea::AddBay(Bay &b) 
{
	internalBays.push_back((WaterArea *) &b);
}
Sea::Sea() : WaterArea()
{
}
Sea::Sea(vector<Bay *> bays) : WaterArea()
{
	for (int i = 0; i < bays.size(); i++)
	{
	
		internalBays.push_back((WaterArea *)bays.back());
		bays.pop_back();
	}
}
Sea::Sea(string name, double area, double lat, double lon, double deep) : WaterArea(name, area, lat, lon, deep)
{
}
Sea::Sea(string name, vector<WaterArea*> bays, double area, double lat, double lon , double deep) : WaterArea(name, area, lat, lon, deep)
{
	this->internalBays = bays;
}
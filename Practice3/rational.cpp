#include "rational.h"
#include <numeric>
#include <iostream>

rational::rational() {
	set(0, 1);
}

rational::rational(int a1, int b1) {
	set(a1, b1);
}
void rational::set(int a1, int b1) {
	b1 = b1 != 0 ? b1 : 1;
	if (a1 > b1)
		a1 = a1 % b1;
	a = a1 / std::gcd(a1, b1);
	b = b1 / std::gcd(a1, b1);
	
}
void rational::show() {
	std::cout << a << "/" << b << std::endl;
}

void rational::operator+(rational other_rational) {
	int a_tmp, b_tmp;	
	a_tmp = a * std::lcm(b, other_rational.b) / b + other_rational.a * std::lcm(b, other_rational.b) / other_rational.b;
	b_tmp = std::lcm(b, other_rational.b);
	set(a_tmp, b_tmp);
}


rational& rational::operator++ ()
{
	set(a + 1, b);
	return *this;
}

rational rational::operator++ (int)
{
	rational copy{ *this };
	++(*this);
	return copy;
}

rational * operator-(rational &x, rational &y)
{
	int a_tmp, b_tmp;
	a_tmp = x.a * std::lcm(x.b, y.b) / y.b - y.a * std::lcm(x.b, y.b) / y.b;
	b_tmp = std::lcm(x.b, y.b);
	return new rational(a_tmp, b_tmp);
}

bool operator==(rational& x, rational& y)
{
	return x.a * std::lcm(x.b, y.b) / y.b == y.a * std::lcm(x.b, y.b) / y.b;
}

bool operator>(rational& x, rational& y)
{
	return x.a * std::lcm(x.b, y.b) / y.b > y.a * std::lcm(x.b, y.b) / y.b;
}

void rational::operator=(rational& x)
{
	set(x.a, x.b);
}
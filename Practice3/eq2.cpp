#include "eq2.h"
#include <math.h>
#include <iostream>


eq2::eq2(double a1, double b1, double c1) {
	set(a1, b1, c1);
}
void eq2::set(double a1, double b1, double c1) {
	a = a1;
	b = b1;
	c = c1;
	D = pow(b, 2) - 4 * a * c;
}
double eq2::find_X() {
	double x1, x2;
	if (D < 0)
	{
		std::cout << "��� �������������� ������." << std::endl;
		return 0;
	}
	x1 = (-b + sqrt(D)) / (2 * a);
	x2 = (-b - sqrt(D)) / (2 * a);

	if (x1 > x2)
		return x1;
	else
		return x2;
}
double eq2::find_Y(double x1) {
	return a * pow(x1, 2) + b * x1 + c;
}

void eq2::operator+(eq2 sec_eq) {
	a += sec_eq.a;
	b += sec_eq.b;
	c += sec_eq.c;
	D = pow(b, 2) - 4 * a * c;
}
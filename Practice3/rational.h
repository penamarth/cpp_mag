#pragma once


class rational
{

private:
	int a, b;
public:
	rational();
	rational(int a1, int b1);
	void set(int a1, int b1);
	void show();
	void operator+(rational other_rational);
	rational& operator++ ();
	rational operator++ (int);
	friend rational * operator-(rational &x, rational &y);
	friend bool operator==(rational& x, rational& y);
	friend bool operator>(rational& x, rational& y);
	void operator=(rational& x);
};
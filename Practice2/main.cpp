#include "triangle.h"
#include "circle.h"
#include "figure.h"
#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");

	/*������� 1*/
	
	triangle *t = new triangle[3];
	double a, b, c;
	
	for (int i = 0; i < 3; i++)
	{
		
		bool valid = false;
		while (!valid)
		{
			cout << "������� ������� � ������������ " << i+1 << endl;
			cin >> a;
			cout << "������� ������� B ������������ " << i+1 << endl;
			cin >> b;
			cout << "������� ������� C ������������ " << i+1 << endl;
			cin >> c;

			t[i].set(a, b, c);
			valid = t[i].exst_tr();
			if (!valid) cout << "����������� �� ����������" << endl;
		}
	}

	for (int i = 0; i < 3; i++)
	{
		cout << "�������� ������������ " << i+1 << " " << t[i].perimetr() << endl;
		cout << "������� ������������ " << i+1 << " " << t[i].square() << endl;
	}
	
	delete[] t;
	
	/*������� 2*/
	
	float x,y,r;

	cout << "������� ���������� ������ ������ ����������: ";
	cin >> x >> y;
	cout << "������� ������ ������ ����������: ";
	cin >> r;

	circle *c1 = new circle(r,x,y);

	cout << "������� ���������� ������ ������ ����������: ";
	cin >> x >> y;
	cout << "������� ������ ������ ����������: ";
	cin >> r;

	circle *c2 = new circle(r, x, y);

	cout << "������� ���������� ������ ������ ����������: ";
	cin >> x >> y;
	cout << "������� ������ ������ ����������: ";
	cin >> r;

	circle *c3 = new circle(r, x, y);

	cout << "������� �����������: " << endl;
	cout << c1->square() << endl;
	cout << c2->square() << endl;
	cout << c3->square() << endl;


	cout << "������� ����� ���������� ������ ������ ����������: ";
	cin >> x >> y;
	cout << "������� ����� ������ ������ ����������: ";
	cin >> r;
	c1->set_circle(r, x, y);
		
	cout << "������� �����������: " << endl;
	cout << c1->square() << endl;
	cout << c2->square() << endl;
	cout << c3->square() << endl;

	cout << "������� ������� � ������������ " << endl;
	cin >> a;
	cout << "������� ������� B ������������ " << endl;
	cin >> b;
	cout << "������� ������� C ������������ " << endl;
	cin >> c;

	if (c1->triangle_around(a,b,c))
	{
		cout << "������ ���������� ����� ���� ������� � �����������" << endl;
	}
	if (c2->triangle_around(a, b, c))
	{
		cout << "������ ���������� ����� ���� ������� � �����������" << endl;
	}
	if (c3->triangle_around(a, b, c))
	{
		cout << "������ ���������� ����� ���� ������� � �����������" << endl;
	}

	if (c1->triangle_in(a, b, c))
	{
		cout << "������ ���������� ����� ���� ������� ������ ������������" << endl;
	}
	if (c2->triangle_in(a, b, c))
	{
		cout << "������ ���������� ����� ���� ������� ������ ������������" << endl;
	}
	if (c3->triangle_in(a, b, c))
	{
		cout << "������ ���������� ����� ���� ������� ������ ������������" << endl;
	}
	
	cout << "������� ���������� ������ ����������� ����������: ";
	cin >> x >> y;
	cout << "������� ������ ����������� ����������: ";
	cin >> r;

	if (c1->check_circle(r, x, y))
	{
		cout << "������ ���������� ������������ � �����������" << endl;
	}
	else
	{
		cout << "������ ���������� �� ������������ � �����������" << endl;
	}
	if (c2->check_circle(r, x, y))
	{
		cout << "������ ���������� ������������ � �����������" << endl;
	}
	else
	{
		cout << "������ ���������� �� ������������ � �����������" << endl;
	}
	if (c3->check_circle(r, x, y))
	{
		cout << "������ ���������� ������������ � �����������" << endl;
	}
	else
	{
		cout << "������ ���������� �� ������������ � �����������" << endl;
	}

	delete c1,c2,c3;


	

	/*������� 3*/
	float  Ax, Ay, Bx, By, Cx, Cy, Dx, Dy;

	cout << "������� ���������� ������� � ��������������� 1: ";
	cin >> Ax >> Ay;
	cout << "������� ���������� ������� B ��������������� 1: ";
	cin >> Bx >> By;
	cout << "������� ���������� ������� C ��������������� 1: ";
	cin >> Cx >> Cy;
	cout << "������� ���������� ������� D ��������������� 1: ";
	cin >> Dx >> Dy;

	figure* fig1 = new figure(Ax,Bx,Cx,Dx,Ay,By,Cy,Dy);
	
	cout << "������� ���������� ������� � ��������������� 2: ";
	cin >> Ax >> Ay;
	cout << "������� ���������� ������� B ��������������� 2: ";
	cin >> Bx >> By;
	cout << "������� ���������� ������� C ��������������� 2: ";
	cin >> Cx >> Cy;
	cout << "������� ���������� ������� D ��������������� 2: ";
	cin >> Dx >> Dy;

	figure* fig2 = new figure(Ax, Bx, Cx, Dx, Ay, By, Cy, Dy);

	cout << "������� ���������� ������� � ��������������� 3: ";
	cin >> Ax >> Ay;
	cout << "������� ���������� ������� B ��������������� 3: ";
	cin >> Bx >> By;
	cout << "������� ���������� ������� C ��������������� 3: ";
	cin >> Cx >> Cy;
	cout << "������� ���������� ������� D ��������������� 3: ";
	cin >> Dx >> Dy;

	figure* fig3 = new figure(Ax, Bx, Cx, Dx, Ay, By, Cy, Dy);
	
	if (fig1->is_prug())
	{
		cout << "�������������� 1 - �������������" << endl;
	}
	
	if (fig2->is_prug())
	{
		cout << "�������������� 2 - �������������" << endl;
	}
	if (fig3->is_prug())
	{
		cout << "�������������� 3 - �������������" << endl;
	}
	
	if (fig1->is_romb())
	{
		cout << "�������������� 1 - ����" << endl;
	}
	
	if (fig2->is_romb())
	{
		cout << "�������������� 2 - ����" << endl;
	}
	if (fig3->is_romb())
	{
		cout << "�������������� 3 - ����" << endl;
	}
	
	if (fig1->is_square())
	{
		cout << "�������������� 1 - �������" << endl;
	}
	
	if (fig2->is_square())
	{
		cout << "�������������� 2 - �������" << endl;
	}
	if (fig3->is_square())
	{
		cout << "�������������� 3 - �������" << endl;
	}
	
	if (fig1->is_in_circle())
	{
		cout << "� �������������� 1 ����� ������� ����������" << endl;
	}
	if (fig2->is_in_circle())
	{
		cout << "� �������������� 2 ����� ������� ����������" << endl;
	}
	if (fig3->is_in_circle())
	{
		cout << "� �������������� 3 ����� ������� ����������" << endl;
	}
	
	if (fig1->is_out_circle())
	{
		cout << "������ ��������������� 1 ����� ������� ����������" << endl;
	}
	if (fig2->is_out_circle())
	{
		cout << "������ ��������������� 2 ����� ������� ����������" << endl;
	}
	if (fig3->is_out_circle())
	{
		cout << "������ ��������������� 3 ����� ������� ����������" << endl;
	}
	
	fig1->show();
	fig2->show();
	fig3->show();

	delete fig1, fig2, fig3;

	return 0;
}
#pragma once
#include <map>
#include <string>
#include "Attribute.h"


class GNode
{
	int _id;
	std::string _type;
	std::string _name;
	std::vector<Attribute *> _attributes;

public:
	GNode(int id);
	GNode(int id, std::string name);
	int getId();
	void addAttribute(Attribute *attribuite);
	void setType(std::string type);
	std::string getType();
	std::string getName();
	Attribute *getAttribute(int i);
	int attribureCount();
};
#include "DBDriver.h"
#include <curl/curl.h>
#include <string>
#include "GNode.h"


size_t WriteCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

DBDriver* DBDriver::instance = nullptr;

DBDriver* DBDriver::getInstance()
{
	if (instance == nullptr)
	{
		instance = new DBDriver();
	}
	return instance;
}

DBDriver::DBDriver()
{
	login = "neo4j";
	pass = "11235813";
	url = "http://localhost:7474/db/neo4j/tx/commit/";
	req = curl_easy_init();
	if (req)
	{
		curl_easy_setopt(req, CURLOPT_URL, url.c_str());
		curl_easy_setopt(req, CURLOPT_HTTPHEADER, curl_slist_append(NULL, "Content-Type:application/json;charset=UTF-8"));
	}
}

DBDriver::~DBDriver()
{
	curl_easy_cleanup(req);
}


std::string DBDriver::sendRequest(std::string cypher)
{
	std::string readBuffer;

	curl_easy_setopt(req, CURLOPT_WRITEDATA, &readBuffer);
	curl_easy_setopt(req, CURLOPT_WRITEFUNCTION, WriteCallback);
	//curl_easy_setopt(req, CURLOPT_VERBOSE, 1);


	curl_easy_setopt(req, CURLOPT_POSTFIELDS, cypher.c_str());
	res = curl_easy_perform(req);

	if (res != CURLE_OK)
	{
		fprintf(stderr, "curl_easy_operation() failed : %s\n", curl_easy_strerror(res));
		return nullptr;
	}

	return readBuffer;
}
	
std::string DBDriver::getNodeById(int id) { 
	return sendRequest("{\"statements\": [{\"statement\": \"MATCH (n) WHERE id(n)=" + std::to_string(id) + " return n,labels(n)\"}]}");
}

std::string DBDriver::getLinkById(int id) { 
	return sendRequest("{\"statements\": [{\"statement\": \"MATCH (i)-[r]->(o) WHERE id(r)=" + std::to_string(id) + " return r,type(r),id(i),id(o)\"}]}");
}

std::string DBDriver::getNodesByLabel(std::string label) { 
	return sendRequest("{\"statements\": [{\"statement\": \"MATCH (n) WHERE n:" + label + " return id(n),labels(n),n\"}]}");
}

std::string DBDriver::getLinksOfNode(int nodeId, bool in) {
	if(in) return sendRequest("{\"statements\": [{\"statement\": \"MATCH (curr)<-[r]-(k) WHERE id(curr)=" + std::to_string(nodeId) + " return id(r),r,type(r),id(k),labels(k)\"}]}");
	else return sendRequest("{\"statements\": [{\"statement\": \"MATCH (curr)-[r]->(k) WHERE id(curr)=" + std::to_string(nodeId) + " return id(r),r,type(r),id(k),labels(k)\"}]}");
}

std::string DBDriver::getNeighborsOfNode(int nodeId, bool in, std::string linkLabel) {
	if (in) return sendRequest("{\"statements\": [{\"statement\": \"MATCH (curr)<-[r]-(n) WHERE id(curr)=" + std::to_string(nodeId) + " AND type(r) = '" + linkLabel + "' return id(n),labels(n),n\"}]}");
	else return sendRequest("{\"statements\": [{\"statement\": \"MATCH (curr)-[r]->(n) WHERE id(curr)=" + std::to_string(nodeId) + " AND type(r) = '" + linkLabel + "' return id(n),labels(n),n\"}]}");
}


#include "Link.h"
#include "GNode.h"

Link::Link(int id, int startNodeId, int endNodeId) {
	_id = id;
	_startNodeId = startNodeId;
	_endNodeId = endNodeId;
}

int Link::getId() { return _id; }

void Link::setType(std::string type) { _type = type; }

std::string Link::getType() { return _type; }

int Link::getStartNodeId() { return _startNodeId; }

int Link::getEndNodeId() { return _endNodeId; }
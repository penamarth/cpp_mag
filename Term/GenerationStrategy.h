#pragma once
#include <string>
#include "Graph.h"
#include <iostream>
#include <fstream>

using std::ostream;

class GenerationStrategy
{
public:
	virtual bool generate(Graph *graph, ostream* out) = 0;
};

class PlantUMLGenerationStrategy : public GenerationStrategy
{
public:
	bool generate(Graph *graph, ostream* out);
};
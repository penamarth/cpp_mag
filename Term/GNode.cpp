#include "GNode.h"

GNode::GNode(int id) {_id = id;}
GNode::GNode(int id, std::string name) { _id = id; _name = name; }

int GNode::getId(){	return _id;}

void GNode::addAttribute(Attribute* attribuite) { _attributes.push_back(attribuite); }


void GNode::setType(std::string type) {	_type = type; }

std::string GNode::getType() { return _type;}

std::string GNode::getName() { return _name; }

Attribute* GNode::getAttribute(int i) { return _attributes[i]; }

int GNode::attribureCount() { return _attributes.size(); }
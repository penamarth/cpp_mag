#pragma once

#include <string>
#include "Graph.h"
#include "DBDriver.h"
#include "GenerationStrategy.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/document.h"

class Translator
{
	GenerationStrategy *_strategy;
	Graph *_graph;
	bool loadClasses();
	bool loadContent();
	bool loadLinks();
	bool loadInterfaces();

public:
	bool load();
	void translate(std::string fname);
	Translator(GenerationStrategy *strategy);
	virtual ~Translator();
	rapidjson::Document *Parse(std::string input);
};
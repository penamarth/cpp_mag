#include "Translator.h"
#include "DBDriver.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/document.h"

#include <iostream>
#include <fstream>

Translator::Translator(GenerationStrategy *strategy) { _graph = new Graph(); _strategy = strategy; }

Translator::~Translator() { delete this->_graph; }

bool Translator::load()
{
	if (!loadClasses()) return false;
	std::cout << "������ ������� ���������" << std::endl;
	if (!loadInterfaces()) return false;
	std::cout << "������ ����������� ���������" << std::endl;
	if (!loadContent()) return false;
	std::cout << "������ ���������� ���������" << std::endl;
	if (!loadLinks()) return false;
	std::cout << "������ ������ ���������" << std::endl;
	return true;
}

bool Translator::loadClasses() {
	DBDriver* db = DBDriver::getInstance();
	rapidjson::Document json;
	string jsontxt = db->getNodesByLabel("CLASS");
	json.Parse(jsontxt.c_str());

	rapidjson::Value& data = json["results"][0]["data"];
	for (rapidjson::SizeType i = 0; i < data.Capacity();i++)
	//for (rapidjson::Value::ConstValueIterator itr = data.Begin(); itr != data.End(); ++itr)
	{
		rapidjson::Value& dataResponce = data[i];
		
		int id = dataResponce["row"][0].GetInt();
		GNode* newNode = new GNode(id, dataResponce["row"][2]["name"].GetString());
		newNode->setType("CLASS");
		_graph->addNode(newNode);
	}
	return true;
}
bool Translator::loadInterfaces() {
	DBDriver* db = DBDriver::getInstance();
	rapidjson::Document json;
	json.Parse(db->getNodesByLabel("INTERFACE").c_str());

	rapidjson::Value& data = json["results"][0]["data"].GetArray();
	for (rapidjson::SizeType i = 0; i < data.Capacity(); i++)
	{
		int id = data[i]['row'][0].GetInt();
		GNode* newNode = new GNode(id);
		newNode->setType("INTERFACE");
		_graph->addNode(newNode);
	}
	return true;
}
bool Translator::loadContent() {
	DBDriver* db = DBDriver::getInstance();
	IGraphNodeIterator* ite = _graph->getClassIterator();

	for (GNode* currNode = ite->begin(); ite->hasNext(); currNode = ite->getNext())
	{
		rapidjson::Document neighbors;
		string jsontxt = db->getNeighborsOfNode(currNode->getId(), true, "DEFINED_IN");
		neighbors.Parse(jsontxt.c_str());

		rapidjson::Value& neighborsData = neighbors["results"][0]["data"].GetArray();
		for (rapidjson::SizeType i = 0; i < neighborsData.Capacity(); i++)
		{
			rapidjson::Value& dataResponce = neighborsData[i];
			if (neighborsData[i]["row"][1][0] == "CONSTRUCTOR")
			{
				Constructor* newAttribute = new Constructor(neighborsData[i]["row"][2]["name"].GetString());
				newAttribute->setAccess("private");
				if(neighborsData[i]["row"][2].HasMember("access"))
					newAttribute->setAccess(neighborsData[i]["row"][2]["access"].GetString());
				rapidjson::Document paramLinks;
				paramLinks.Parse(db->getNeighborsOfNode(neighborsData[i]["row"][0].GetInt(), false, "EXPECTS").c_str());
				rapidjson::Value& paramData = paramLinks["results"][0]["data"];
				for (rapidjson::SizeType j = 0; j < paramData.Capacity(); j++)
				{
					newAttribute->addParameter(paramData[j][2]["name"].GetString());
				}
				currNode->addAttribute(newAttribute);
				continue;
			}
			if (neighborsData[i]["row"][1][0] == "METHOD")
			{
				rapidjson::Document returnParam;
				jsontxt = db->getNeighborsOfNode(neighborsData[i]["row"][0].GetInt(), false, "RETURNS");
				returnParam.Parse(jsontxt.c_str());

				std::string name = neighborsData[i]["row"][2]["name"].GetString();
				std::string returnType = returnParam["results"][0]["data"][0]["row"][2]["name"].GetString();
				Method* newAttribute = new Method(name, returnType);
				rapidjson::Document paramLinks;
				jsontxt = db->getNeighborsOfNode(neighborsData[i]["row"][0].GetInt(), false, "EXPECTS");
				paramLinks.Parse(jsontxt.c_str());
				rapidjson::Value& paramData = paramLinks["results"][0]["data"];
				for (rapidjson::SizeType j = 0; j < paramData.Capacity(); j++)
				{
					string param = paramData[j]["row"][2]["name"].GetString();
					newAttribute->addParameter(param);
				}
				currNode->addAttribute(newAttribute);
				continue;
			}

			if (neighborsData[i]["row"][1][0] == "FIELD")
			{
				rapidjson::Document type;
				type.Parse(db->getNeighborsOfNode(neighborsData[i]["row"][0].GetInt(), false, "TYPE_OF").c_str());

				Field* newAttribute = new Field(neighborsData[i]["row"][2]["name"].GetString(), type["results"][0]["data"][0]["row"][2]["name"].GetString());
				
				currNode->addAttribute(newAttribute);
				continue;
			}

			if (neighborsData[i]["row"][1][0] == "CLASS")
			{
				continue;
			}

		}
	}

	return true;
}

bool Translator::loadLinks() {
	DBDriver* db = DBDriver::getInstance();
	IGraphNodeIterator* nodeIte = _graph->getClassIterator();

	for (GNode* currNode = nodeIte->begin(); nodeIte->hasNext(); currNode = nodeIte->getNext())
	{
		rapidjson::Document inLinks, outLinks;
		string jsontxt = db->getLinksOfNode(currNode->getId(), true);
		inLinks.Parse(jsontxt.c_str());
		rapidjson::Value& inLinksData = inLinks["results"][0]["data"].GetArray();

		for (rapidjson::SizeType i = 0; i < inLinksData.Capacity(); i++)
		{
			if (_graph->getLinkById(inLinksData[i]["row"][0].GetInt()) == nullptr &&
				(inLinksData[i]["row"][4][0] == "CLASS" ||
					inLinksData[i]["row"][4][0] == "INTERFACE"))
			{
				Link* link = new Link(inLinksData[i]["row"][0].GetInt(), inLinksData[i]["row"][3].GetInt(), currNode->getId());
				link->setType(inLinksData[i]["row"][2].GetString());
				_graph->addLink(link);
			}
		}

		jsontxt = db->getLinksOfNode(currNode->getId(), false);
		outLinks.Parse(jsontxt.c_str());
		rapidjson::Value& outLinksData = outLinks["results"][0]["data"].GetArray();

		for (rapidjson::SizeType i = 0; i < outLinksData.Capacity(); i++)
		{
			if (_graph->getLinkById(outLinksData[i]["row"][0].GetInt()) == nullptr &&
				(outLinksData[i]["row"][4][0] == "CLASS" ||
					outLinksData[i]["row"][4][0] == "INTERFACE"))
			{
				Link* link = new Link(outLinksData[i]["row"][0].GetInt(), currNode->getId(), outLinksData[i]["row"][3].GetInt());
				link->setType(outLinksData[i]["row"][2].GetString());
				_graph->addLink(link);
			}
		}
	}


	return true;
}

void Translator::translate(std::string fname)
{
	std::ostream* out;
	if (fname == "")
	{
		out = &std::cout;
	}
	else
	{
		out = new std::ofstream(fname);
	}

	if(_strategy->generate(_graph, out))
		std::cout << "���������� ���������." << std::endl;
	if (fname != "")
	{
		((std::ofstream*)out)->close();
		std::cout << "���� " << fname << " �������." << std::endl;
		delete out;
	}
}

rapidjson::Document* Translator::Parse(std::string input)
{
	rapidjson::Document* doc = new rapidjson::Document();
	if (doc->Parse(input.c_str()).HasParseError())
		return nullptr;

	return doc;
}


#pragma once
#include "GNode.h"

class Link
{
	int _id;
	int _startNodeId;
	int _endNodeId;
	std::string _type;
public:
	Link(int id, int startNodeId, int endNodeId);
	int getId();
	void setType(std::string type);
	std::string getType();
	int getStartNodeId();
	int getEndNodeId();
};
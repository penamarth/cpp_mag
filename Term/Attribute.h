#pragma once
#include <string>
#include <vector>
#include <map>
using std::string;

enum AttributeKind { METHOD, FIELD, CONSTRUCTOR };
enum AttributeAccess { PRIVATE, PROTECTED, PUBLIC };

class Attribute
{
protected:
	std::string _name;
	AttributeAccess _access;

public:
	virtual AttributeKind Kind() = 0;
	Attribute(std::string name) { _name = name; _access = PUBLIC; }
	virtual ~Attribute() {}
	std::string getName() { return _name; }
	void setAccess(string access)
	{
		if (access == "public") _access = PUBLIC;
		if (access == "private") _access = PRIVATE;
		if (access == "protected") _access = PROTECTED;
	}
	AttributeAccess getAccess() { return _access; }
};

class Method : public Attribute
{
	std::string _returnType;
	std::vector<string> _parameters;
public:
	Method(std::string name, std::string returnType) :Attribute(name) { _returnType = returnType; }
	AttributeKind Kind() { return AttributeKind::METHOD; }
	void setReturnType(std::string returnType) { _returnType = returnType; }
	std::string getReturnType() { return _returnType; }
	void addParameter(std::string type)
	{
		_parameters.push_back(type);
	}
	std::string getParameter(int id) { return _parameters[id]; }
	int parameterCount() { return _parameters.size(); }
};

class Field : public Attribute
{
	std::string _type;
public:
	Field(std::string name, std::string type) :Attribute(name) { _type = type; }
	AttributeKind Kind() { return AttributeKind::FIELD; }
	std::string getType() { return _type; }
};

class Constructor : public Attribute
{
	std::vector<string> _parameters;

public:
	AttributeKind Kind() { return AttributeKind::CONSTRUCTOR; }
	Constructor(std::string name) : Attribute(name) { }
	std::string getReturnType() { return _name; }
	void addParameter(std::string type)
	{
		_parameters.push_back(type);
	}
	std::string getParameter(int id) { return _parameters[id]; }
	int parameterCount() { return _parameters.size(); }
};
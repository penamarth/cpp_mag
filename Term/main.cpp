#include <iostream>
#include <curl/curl.h>
#include <string>
#include "Translator.h"

using namespace std;

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "Russian");
	string fname = "";
	if (argc == 2)
		fname = argv[1];

	Translator tr(new PlantUMLGenerationStrategy());
	if (tr.load()) {
		cout << "Загрузка завершена" << endl;
		tr.translate(fname);
		return 0;
	}
	else return 1;
	
}
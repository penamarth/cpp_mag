#include "Graph.h"


Graph::Graph() {
}
Graph::~Graph() {
}
void Graph::addNode(GNode *node)
{
	_nodes.push_back(node);
}
void Graph::addLink(Link* link){
	_links.push_back(link);
}

GNode* Graph::getNodeByIndex(int index) {
	if (index < _nodes.size()) return _nodes[index];
	else return nullptr;
}

GNode* Graph::getNodeById(int nodeId)
{
	for (int i = 0; i < _nodes.size(); i++)
	{
		if (_nodes[i]->getId() == nodeId)
			return _nodes[i];
	}
	return nullptr;
}

Link* Graph::getLinkByIndex(int index)
{
	if (index + 1 < _nodes.size()) return _links[index];
	else return nullptr;
}

Link* Graph::getLinkById(int linkId)
{
	for (int i = 0; i < _links.size(); i++)
	{
		if (_links[i]->getId() == linkId)
			return _links[i];
	}
	return nullptr;
}

IGraphNodeIterator *Graph::getClassIterator() {
	return new GraphClassIterator(this);
}

IGraphNodeIterator *Graph::getInterfaceIterator() {
	return new GraphInterfaceIterator(this);
}

IGraphLinkIterator *Graph::getLinkOfNodeIterator(int nodeId) {
	return new GraphLinkOfNodeIterator(this, nodeId);
}

IGraphLinkIterator *Graph::getAllLinkIterator(std::string type) {
	return new GraphAllLinkIterator(this, type);
}

GraphClassIterator::GraphClassIterator(Graph* graph) {
	_graph = graph;
	_innerId = 0;
}
GNode* GraphClassIterator::begin()
{
	_innerId = 0;
	while (GNode* res = _graph->getNodeByIndex(_innerId))
	{
		if (res->getType() == "CLASS")
			return res;
		_innerId++;
	}
	return nullptr;
}
bool GraphClassIterator::hasNext() {
	int tmpId = _innerId;
	while (GNode* res = _graph->getNodeByIndex(tmpId))
	{
		if (res->getType() == "CLASS")
			return true;
		tmpId++;
	}
	return false;
}
GNode* GraphClassIterator::getNext() {
	_innerId++;
	while (GNode* res = _graph->getNodeByIndex(_innerId))
	{
		if (res->getType() == "CLASS")
			return res;
		_innerId++;
	}
	return nullptr;
}


GraphInterfaceIterator::GraphInterfaceIterator(Graph* graph) {
	_graph = graph;
	_innerId = 0;
}
GNode* GraphInterfaceIterator::begin()
{
	_innerId = 0;
	while (GNode* res = _graph->getNodeByIndex(_innerId))
	{
		if (res->getType() == "INTERFACE")
			return res;
		_innerId++;
	}
	return nullptr;
}
bool GraphInterfaceIterator::hasNext() {
	int tmpId = _innerId;
	while (GNode* res = _graph->getNodeByIndex(tmpId))
	{
		if (res->getType() == "INTERFACE")
			return true;
		tmpId++;
	}
	return false;
}
GNode* GraphInterfaceIterator::getNext() {
	_innerId++;
	while (GNode* res = _graph->getNodeByIndex(_innerId))
	{
		if (res->getType() == "INTERFACE")
			return res;
		_innerId++;
	}
	return nullptr;
}




GraphLinkOfNodeIterator::GraphLinkOfNodeIterator(Graph* graph, int nodeId) {
	_graph = graph;
	_linkId = 0;
	_nodeId = nodeId;
}
Link* GraphLinkOfNodeIterator::begin()
{
	_linkId = 0;
	while (Link* res = _graph->getLinkByIndex(_linkId))
	{
		if (res->getStartNodeId() == _nodeId || res->getEndNodeId() == _nodeId)
			return res;
		_nodeId++;
	}
	return nullptr;
}
bool GraphLinkOfNodeIterator::hasNext() {
	int tmpId = _linkId;
	while (Link* res = _graph->getLinkByIndex(tmpId))
	{
		if (res->getStartNodeId() == _nodeId || res->getEndNodeId() == _nodeId)
			return true;
		tmpId++;
	}
	return false;
}
Link* GraphLinkOfNodeIterator::getNext() {
	_linkId++;
	while (Link* res = _graph->getLinkByIndex(_linkId))
	{
		if (res->getStartNodeId() == _nodeId || res->getEndNodeId() == _nodeId)
			return res;
		_linkId++;
	}
	return nullptr;
}




GraphAllLinkIterator::GraphAllLinkIterator(Graph* graph, std::string type = "0") {
	_graph = graph;
	_linkId = -1;
	_type = type;

}
Link* GraphAllLinkIterator::begin()
{
	_linkId = 0;
	while (Link* res = _graph->getLinkByIndex(_linkId))
	{
		if (_type == "" || res->getType() == _type)
			return res;
		_linkId++;
	}
	return nullptr;
}
bool GraphAllLinkIterator::hasNext() {
	int tmpId = _linkId;
	while (Link* res = _graph->getLinkByIndex(tmpId))
	{
		if (_type == "" || res->getType() == _type)
			return true;
		tmpId++;
	}
	return false;
}
Link* GraphAllLinkIterator::getNext() {
	_linkId++;
	while (Link* res = _graph->getLinkByIndex(_linkId))
	{
		if (_type == "" || res->getType() == _type)
			return res;
	}
	return nullptr;
}


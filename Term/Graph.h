#pragma once
#include "GNode.h"
#include "Link.h"
#include <vector>


class IGraphNodeIterator;
class IGraphLinkIterator;

class Graph
{
	std::vector<GNode *> _nodes;
	std::vector<Link *> _links;

public:
	Graph();
	virtual ~Graph();
	void addNode(GNode *node);
	void addLink(Link *link);
	GNode *getNodeByIndex(int index);
	GNode *getNodeById(int nodeId);
	Link *getLinkByIndex(int index);
	Link *getLinkById(int linkId);
	IGraphNodeIterator *getClassIterator();
	IGraphNodeIterator *getInterfaceIterator();
	IGraphLinkIterator *getLinkOfNodeIterator(int nodeId);
	IGraphLinkIterator *getAllLinkIterator(std::string type = "");
};

class IGraphNodeIterator
{
public:
	virtual GNode* begin() = 0;
	virtual GNode* getNext() = 0;
	virtual bool hasNext() = 0;
	virtual ~IGraphNodeIterator() {};
};

class GraphClassIterator : public IGraphNodeIterator
{
private:
	Graph* _graph;
	int _innerId;
public:
	GraphClassIterator(Graph* raph);
	virtual GNode* begin();
	virtual GNode* getNext();
	virtual bool hasNext();
};

class GraphInterfaceIterator : public IGraphNodeIterator
{
private:
	Graph* _graph;
	int _innerId;
public:
	GraphInterfaceIterator(Graph* graph);
	virtual GNode* begin();
	virtual GNode* getNext();
	virtual bool hasNext();
};

class IGraphLinkIterator
{
public:
	virtual Link* begin() = 0;
	virtual Link* getNext() = 0;
	virtual bool hasNext() = 0;
	virtual ~IGraphLinkIterator() {};
};

class GraphLinkOfNodeIterator : public IGraphLinkIterator
{
private:
	Graph* _graph;
	int _nodeId;
	int _linkId;
public:
	GraphLinkOfNodeIterator(Graph* graph, int nodeId);
	virtual Link* begin();
	virtual Link* getNext();
	virtual bool hasNext();
};

class GraphAllLinkIterator : public IGraphLinkIterator
{
private:
	Graph* _graph;
	int _linkId;
	std::string _type;
public:
	GraphAllLinkIterator(Graph* graph, std::string type);
	virtual Link* begin();
	virtual Link* getNext();
	virtual bool hasNext();
};
#pragma once
#include <string>
#include "Graph.h"
#include "GNode.h"
#include "Link.h"
#include <curl/curl.h>


class DBDriver
{
	CURL *req;
	CURLcode res;
	std::string login;
	std::string pass;
	std::string url;
	static DBDriver *instance;
	std::string sendRequest(std::string cypher);

	DBDriver();
	virtual ~DBDriver();

public:
	static DBDriver *getInstance();
	std::string getNodeById(int id);
	std::string getLinkById(int id);
	std::string getNodesByLabel(std::string label);
	std::string getLinksOfNode(int nodeId, bool in);
	std::string getNeighborsOfNode(int nodeId, bool in, std::string linkLabel);
};


#include "GenerationStrategy.h"
#include "Graph.h"


bool PlantUMLGenerationStrategy::generate(Graph *graph, ostream* out)
{
	std::string result = "@startuml\n";
	IGraphNodeIterator* ite = graph->getClassIterator();

	for (GNode *currNode = ite->begin(); ite->hasNext(); currNode = ite->getNext())
	{
		result += "class ";
		result += currNode->getName();
		result += "\n{\n";

		for (int i = 0; i < currNode->attribureCount(); i++)
		{
			Attribute *attr = currNode->getAttribute(i);
			switch (attr->getAccess())
			{
			case PRIVATE:
				result += "-";
				break;
			case PROTECTED:
				result += "#";
				break;
			case PUBLIC:
				result += "+";
				break;
			default:
				result += "-";
			}
			if (attr->Kind() == AttributeKind::FIELD)
			{
				result += attr->getName();
				result += " : ";
				result += ((Field*)attr)->getType();
				result += "\n";
				continue;
			}
			if (attr->Kind() == AttributeKind::CONSTRUCTOR)
			{
				result += attr->getName();
				result += "( ";
				for (int j = 0; j < ((Constructor*)attr)->parameterCount(); j++)
				{
					result += ((Constructor*)attr)->getParameter(j);
					result += ",";
				}
				result.pop_back();
				result += ")\n";
				continue;
			}
			if (attr->Kind() == AttributeKind::METHOD)
			{
				result += attr->getName();
				result += "( ";
				for (int j = 0; j < ((Method*)attr)->parameterCount(); j++)
				{
					result += ((Method*)attr)->getParameter(j);
					result += ",";
				}
				result.pop_back();
				result += ") : ";
				result += ((Method*)attr)->getReturnType();
				result += "\n";
			}
		}
		result += "}\n\n";	
	}

	ite = graph->getInterfaceIterator();

	for (GNode* currNode = ite->begin(); ite->hasNext(); currNode = ite->getNext())
	{
		result += "interface ";
		result += currNode->getName();
		result += "\n{\n";

		for (int i = 0; i < currNode->attribureCount(); i++)
		{
			Attribute* attr = currNode->getAttribute(i);
			if (attr->Kind() == AttributeKind::CONSTRUCTOR)
			{
				result += attr->getName();
				result += "(";
				for (int j = 0; j < ((Constructor*)attr)->parameterCount(); j++)
				{
					result += ((Constructor*)attr)->getParameter(j);
					result += ",";
				}
				result += ")\n";
				continue;
			}
			if (attr->Kind() == AttributeKind::METHOD)
			{
				result += attr->getName();
				result += "(";
				for (int j = 0; j < ((Method*)attr)->parameterCount(); j++)
				{
					result += ((Method*)attr)->getParameter(j);
					result += ",";
				}
				result += ") : ";
				result += ((Method*)attr)->getReturnType();
				result += "\n";
			}
		}
		result += "}\n";
	}

	IGraphLinkIterator* linkIte = graph->getAllLinkIterator();
	for (Link* currLink = linkIte->begin(); linkIte->hasNext(); currLink = linkIte->getNext())
	{
		if (currLink->getType() == "CREATE")
		{
			result += graph->getNodeById(currLink->getStartNodeId())->getName();
			result += " --> ";
			result += graph->getNodeById(currLink->getEndNodeId())->getName();
			result += " : creates";
			result += "\n\n";
			continue;
		}
		if (currLink->getType() == "INHERITS")
		{
			result += graph->getNodeById(currLink->getStartNodeId())->getName();
			result += " --|> ";
			result += graph->getNodeById(currLink->getEndNodeId())->getName();
			result += "\n\n";
			continue;
		}
		if (currLink->getType() == "REALIZES")
		{
			result += graph->getNodeById(currLink->getStartNodeId())->getName();
			result += " ..|> ";
			result += graph->getNodeById(currLink->getEndNodeId())->getName();
			result += "\n\n";
			continue;
		}
		if (currLink->getType() == "ASSOCIATES")
		{
			result += graph->getNodeById(currLink->getStartNodeId())->getName();
			result += " -- ";
			result += graph->getNodeById(currLink->getEndNodeId())->getName();
			result += "\n\n";
			continue;
		}
	}

	result += "@enduml\n";

	*out << result;
	return true;
}
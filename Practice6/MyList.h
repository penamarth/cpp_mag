#pragma once

#include <iostream>
#include <exception>
#include <string>
using namespace std;

template <class T> class MyList
{
	static int count;
	static MyList<T>* head;
	T value;
	MyList<T> *next;
public:
	T operator[](int i)
	{
		try
		{
			if (count == 0) throw exception("List is empty!");
			if (i < 0 || i > count - 1) throw exception("Out of range!");
			MyList<T>* current = head;
			for (int j = 0; j < i; j++)
			{
				current = current->next;		
			}
			return current->value;
		}
		catch (exception& e)
		{
			cout << e.what() << '\n';
		}
		
	}



	int getCount() { return count; }

	MyList<T>()
	{
		count = 0;
		head = nullptr;
	}

	MyList<T>(T _value)
	{
		if (count == 0)
		{
			head = this;
		}
		else
		{
			MyList<T>* last = head;
			for (int j = 0; j < count - 1; j++)
			{
				last = last->next;
			}
			last->next = this;
		}
		this->next = nullptr;
		this->value = _value;
		count++;
	}

	void remove(int i)
	{
		try
		{
			if (count == 0) throw exception("List is empty!");
			if (i < 0 || i > count - 1) throw exception("Out of range!");
			MyList<T>* current = head;
			for (int j = 0; j < i-1; j++)
			{
				current = current->next;
			}
			MyList<T>* tmp = current->next;
			current->next = current->next->next;
			delete tmp;
			count--;
		}
		catch (exception& e)
		{
			cout << e.what() << '\n';
		}
	}

};

template <class T>
int MyList<T>::count = 0;

template <class T>
MyList<T>* MyList<T>::head = nullptr;
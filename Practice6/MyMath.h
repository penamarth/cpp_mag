#pragma once
#include <iostream>
#include <exception>
#include <string>
using namespace std;

template <class T> class MyMathBase
{
protected:
	T op1, op2;
	T result;

public:
	MyMathBase()
	{
		op1 = Null();
		op2 = Null();
		result = Null();
	}

	MyMathBase(T a1, T a2)
	{
		op1 = a1;
		op2 = a2;
		//result = Null();
	}

	virtual bool isNull(T) = 0;
	virtual T Null() = 0;

	virtual T add() = 0;
	virtual T sub() = 0;
	virtual T mul() = 0;
	virtual T div() = 0;
};

class MyMathException : public exception
{
	string error;

public:
	MyMathException(string text)
	{
		error = "MyMath error :";
		error += text;
	}

	virtual const char* what() const throw()
	{
		return error.c_str();
	}
};

template <class T> class MyMathNum : public MyMathBase<T>
{
public:
	MyMathNum() : MyMathBase<T>()
	{
	}
	MyMathNum(T a1, T a2) : MyMathBase<T>(a1, a2)
	{
	}

	bool isNull(T p)
	{
		return (p - (T)0) < 0.00001;
	}

	T Null()
	{
		return (T)0;
	}

	T add()
	{
		return MyMathBase<T>::op1 + MyMathBase<T>::op2;
	}
	T sub()
	{
		return MyMathBase<T>::op1 - MyMathBase<T>::op2;
	}
	T mul()
	{
		return MyMathBase<T>::op1 * MyMathBase<T>::op2;
	}
	T div()
	{
		try
		{
			if (isNull(MyMathBase<T>::op2)) throw MyMathException("Division by zero.");
			return MyMathBase<T>::op1 / MyMathBase<T>::op2;
		}
		catch (exception& e)
		{
			cout << e.what() << '\n';
		}
	}

};
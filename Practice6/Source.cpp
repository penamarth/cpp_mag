#include "MyMath.h"
#include <vector>
#include <exception>
#include <set>
#include "MyList.h"


int main()
{
	/*������� 1*/

	MyMathBase<int>* math = new MyMathNum<int>(1, 2);
	cout << math->add() << endl;
	cout << math->sub() << endl;
	cout << math->mul() << endl;
	cout << math->div() << endl;

	cout << "\n";

	math = new MyMathNum<int>(1, 0);
	cout << math->add() << endl;
	cout << math->sub() << endl;
	cout << math->mul() << endl;
	cout << math->div() << endl;

	cout << "\n";

	MyMathBase<double>* dmath = new MyMathNum<double>(1.0, 0.0);
	cout << dmath->add() << endl;
	cout << dmath->sub() << endl;
	cout << dmath->mul() << endl;
	cout << dmath->div() << endl;


	/*������� 2*/
	vector<int> a;
	a.push_back(1);
	a.push_back(2);
	a.push_back(3);
	a.push_back(4);
	a.push_back(5);

	int i;

	cout << "Enter index: " << endl;
	cin >> i;

	try
	{
		if (i < 0 || i > a.size() - 1) throw exception("Out of range!");
		cout << a[i] << '\n';
	}
	catch (exception& e)
	{
		cout << e.what() << '\n';
	}

	/*������� 3*/
	MyList<char> *list = new MyList<char>();

	cout << (*list)[0] << '\n';

	new MyList<char>('A');
	new MyList<char>('B');
	new MyList<char>('C');



	cout << (*list)[0] << '\n';
	cout << (*list)[1] << '\n';
	cout << (*list)[2] << '\n';

	list->remove(1);

	cout << (*list)[0] << '\n';
	cout << (*list)[1] << '\n';
	cout << (*list)[2] << '\n';

	return 0;
}